from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from tasks.forms import TaskCreateForm, TaskUpdateForm
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskCreateForm(request.POST)
        if form.is_valid():
            task = form.instance
            task.save()
            return redirect("show_project", pk=task.project.id)
    else:
        form = TaskCreateForm()
    context = {"form": form}
    return render(
        request,
        "tasks/create.html",
        context,
    )


@login_required
def list_tasks(request):
    context = {
        "tasks_list": Task.objects.filter(assignee=request.user),
    }
    return render(request, "tasks/list.html", context)


def update_task(request, pk):
    task = Task.objects.filter(pk=pk).get()
    if request.method == "POST":
        form = TaskUpdateForm(request.POST, instance=task)
        if form.is_valid():
            task = form.instance
            task.save()
            return redirect("show_my_tasks")
    else:
        form = TaskUpdateForm(instance=task)
    context = {"form": form}
    return render(request, "tasks/list.html", context)
