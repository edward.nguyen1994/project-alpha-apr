from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from projects.models import Project
from projects.forms import ProjectCreateForm

# Create your views here.


@login_required
def list_projects(request):
    context = {
        "projects_list": Project.objects.filter(members=request.user),
    }
    return render(request, "projects/list.html", context)


@login_required
def detail_project(request, pk):
    context = {
        "projects_detail": Project.objects.filter(pk=pk).get(),
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectCreateForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.save()
            form.save_m2m()
            return redirect("show_project", pk=project.id)
    else:
        form = ProjectCreateForm()
    context = {"form": form}
    return render(
        request,
        "projects/create.html",
        context,
    )
